ARG BASE_IMAGE=
ARG BASE_TAG=
ARG HTTP_PROXY=
ARG HTTPS_PROXY=
ARG http_proxy=
ARG https_proxy=

FROM ${BASE_IMAGE}:${BASE_TAG}
LABEL maintainer="Stefan Schneider <eqsoft4@gmail.com>"

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin

ENV LL_TAG=v7.1.1

RUN <<EOF
set -e
apt-get update
apt-get install -y --no-install-recommends \
tzdata \
apt-transport-https \
ca-certificates \
software-properties-common \
wget \
procps \
curl \
git \
python3 \
build-essential \
xvfb \
apt-transport-https \
unzip \
gettext base \
socat
rm -rf /var/lib/apt/lists/*
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
echo $TZ > /etc/timezone
EOF

COPY files/package.json /opt/package.json
RUN <<EOF
git clone https://github.com/LearningLocker/learninglocker.git /opt/learninglocker
cd /opt/learninglocker
git checkout $LL_TAG
rm package.json
cp /opt/package.json /opt/learninglocker/package.json
npm_config_build_from_source=true yarn install --ignore-engines
yarn build-all
EOF

WORKDIR /opt/learninglocker

RUN cp -r storage storage.template

EXPOSE 3000 8080

COPY files/env.template .env.template
COPY files/entrypoint-common.sh entrypoint-common.sh
COPY files/entrypoint-ui.sh entrypoint-ui.sh

RUN <<EOF
set -e
apt remove -y git curl wget build-essential unzip
apt autoremove -y --purge
apt clean -y
EOF

ENTRYPOINT ["./entrypoint-common.sh"]